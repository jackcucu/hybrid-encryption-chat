var userName;
var to;
var type;
function post(url, data) {
    return $.ajax({
        type: 'POST',
        url: url,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data: JSON.stringify(data)
    })
}
function appendMessage(message) {
    console.log(message);
    var $message = $(`<li class="clearfix">
        <div class="message-data ${message.from == userName ? 'align-right' : 'align-left'}">
        <span class="message-data-name">${message.from}</span>
        <span class="message-data-time">${message.time}</span>
    </div>
    <div class="message ${message.from == userName ? 'my-message' : 'other-message float-left'}">
        ${message.message}
    </div>
    </li>`);
    var $messages = $('#messages');
    $messages.append($message);
    $messages.scrollTop($messages.prop("scrollHeight"));
}

function getPreviousMessages() {
    var request = $.ajax({
        url: '/chat',
        type: 'GET',
        data: { from: userName, to:to, type: type} ,
        contentType: 'application/json; charset=utf-8'
    });
    request.done(messages => messages.forEach(appendMessage));
}

function sendMessage() {
        var $messageInput = $('#messageInput');
        var message = {message: $messageInput.val(), from: userName, to: to, messageType: type};
        post('/chat', message);
    $messageInput.val('');
}
function conversationWithUser(name) {
    to = name;
    type = "USER";
    var $messages = $('#messages');
    var $sendBody = $('#chat');
    $sendBody.show();
    $messages.show();
    $messages.empty();
    getPreviousMessages();
}
function conversationWithGroup(name) {
    to = name;
    type = "GROUP";
    var $messages = $('#messages');
    var $sendBody = $('#chat');
    $sendBody.show();
    $messages.show();
    $messages.empty();
    getPreviousMessages();
}
function onNewMessage(result) {
    var message = JSON.parse(result.body);
    console.log(message);
    appendMessage(message);
}

function connectWebSocket() {
    var socket = new SockJS('/chatWS');
    chatSocket = Stomp.over(socket);
    chatSocket.connect({}, (frame) => {
        userName = frame.headers['user-name'];
        chatSocket.subscribe('/topic/' + userName, onNewMessage);
        chatSocket.subscribe('/exchange/', function (result) {
            toaster.pop('error', "Error", result.body);
        });
        chatSocket.subscribe('/topic/chat.login', function () {
            chatSocket.send("/topic/exchange", {}, frame.headers['user-name']);
        });
        chatSocket.subscribe('/topic/chat.logout', function () {
            console.log("disconnected");
        });
    });
}
connectWebSocket();