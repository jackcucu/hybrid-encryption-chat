'use strict';

angular.module('springChat', ['toaster'])
    .controller('ChatController', ['$scope', 'toaster', '$compile', function ($scope, toaster, $compile) {
        $scope.username = '';
        $scope.sendTo = '';
        $scope.participants = [];
        $scope.type = '';
        $scope.activeChat = '';

        $scope.post = function (url, data) {
            return $.ajax({
                type: 'POST',
                url: url,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify(data)
            })
        };
        $scope.appendMessage = function (message) {
            var $message = $(`<li class="clearfix">
				<div class="message-data ${message.from == $scope.username ? 'align-left' : 'align-right'}">
					<span class="message-data-name">${message.fromName} ${message.fromLastName}</span>
					<span class="message-data-time">${message.time}</span>
				</div>
    			<div class="message ${message.from == $scope.username ? 'my-message' : 'other-message float-right'}">
					${message.message}
    			</div>
    			</li>`);
            var $messages = $('#messages');
            $messages.append($message);
            $messages.scrollTop($messages.prop("scrollHeight"));
        };
        $scope.appendParticipant = function (participant) {
            console.log(participant);
            var element = $compile(`<li>
                        <span class="input-icon fui-user"></span>
                        <a href="" ng-click="conversationWithUser('${participant.username}','${participant.id}');">${participant.name} ${participant.lastName}</a>
                        <span class="navbar-unread" id="unread-${participant.id}" style="display: none">1</span>
                    </li>`)($scope);
            var $participant = $('#participants');
            $participant.append(element);
        };
        $scope.getPreviousMessages = function () {
            var request = $.ajax({
                url: '/chat',
                type: 'GET',
                data: {from: $scope.username, to: $scope.sendTo, type: $scope.type},
                contentType: 'application/json; charset=utf-8'
            });
            request.done(messages => messages.forEach($scope.appendMessage));
        };
        $scope.sendMessage = function () {
            var $messageInput = $('#messageInput');
            var message = {
                message: $messageInput.val(),
                from: $scope.username,
                to: $scope.sendTo,
                messageType: $scope.type
            };
            $scope.post('/chat', message);
            $messageInput.val('');
        };
        $scope.conversationWithUser = function (name, id) {
            $scope.sendTo = name;
            $scope.type = "USER";
            $scope.activeChat = "user" + id;
            var $messages = $('#messages');
            var $sendBody = $('#chat');
            var $unread = $('#unread-' + id);
            $unread.hide();
            $sendBody.show();
            $messages.show();
            $messages.empty();
            $scope.getPreviousMessages();
        };
        $scope.conversationWithGroup = function (name, id) {
            $scope.sendTo = name;
            $scope.type = "GROUP";
            $scope.activeChat = "group" + id;
            var $messages = $('#messages');
            var $sendBody = $('#chat');
            var $groupUnread = $('#group-unread-' + id);
            $groupUnread.hide();
            $sendBody.show();
            $messages.show();
            $messages.empty();
            $scope.getPreviousMessages();
        };
        $scope.onNewMessage = function (result) {
            var message = JSON.parse(result.body);
            console.log(message);
            var $unread = $('#unread-' + message.fromId);
            var $groupUnread = $('#group-unread-' + message.fromId);
            if (message.messageType == "USER") {
                var user = "user" + message.fromId;
                if (message.from != $scope.username && user != $scope.activeChat) {
                    $unread.show();
                }
            } else if (message.messageType == "GROUP") {
                var group = "group" + message.fromId;
                if (group != $scope.activeChat) {
                    $groupUnread.show();
                }
            }
            $scope.appendMessage(message);
        };
        var initWebSocket = function () {
            var chatSocket = Stomp.over(new SockJS('/chatWS'));
            chatSocket.connect({}, (frame) => {
                $scope.username = frame.headers['user-name'];
                chatSocket.subscribe('/topic/' + $scope.username, $scope.onNewMessage);
                chatSocket.subscribe('/exchange/', function (result) {
                });
                chatSocket.subscribe("/topic/chat.login", function (message) {
                    $scope.participants.unshift({
                        username: JSON.parse(message.body).username,
                        name: JSON.parse(message.body).name,
                        lastName: JSON.parse(message.body).lastName,
                        id: JSON.parse(message.body).id
                    });
                    var $participant = $('#participants');
                    var $number = $('#participants-number');
                    $number.empty();
                    $number.append("Online [" + $scope.participants.length + "]");
                    $participant.empty();
                    var $user = $('#user-' + JSON.parse(message.body).id);
                    console.log($user);
                    $user.remove();
                    $scope.participants.forEach($scope.appendParticipant);
                });
                chatSocket.subscribe("/topic/chat.logout", function (message) {
                    console.log("logout");
                    var username = JSON.parse(message.body).username;
                    for (var index in $scope.participants) {
                        if ($scope.participants[index].username == username) {
                            $scope.participants.splice(index, 1);
                        }
                    }
                    var $number = $('#participants-number');
                    $number.empty();
                    $number.append("Online [" + $scope.participants.length + "]");
                    var $participant = $('#participants');
                    $participant.empty();
                    $scope.participants.forEach($scope.appendParticipant);
                });
                chatSocket.subscribe("/app/chat.participants", function (message) {
                    $scope.participants = JSON.parse(message.body);
                    console.log(message);
                    var $number = $('#participants-number');
                    $number.empty();
                    $number.append("Online [" + $scope.participants.length + "]");
                    var $participant = $('#participants');
                    $participant.empty();
                    $scope.participants.forEach($scope.appendParticipant);
                });
            });
        };
        initWebSocket();
    }]);
