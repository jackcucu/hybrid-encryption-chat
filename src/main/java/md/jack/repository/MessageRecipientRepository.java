package md.jack.repository;

import md.jack.domain.MessageRecipient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRecipientRepository extends CrudRepository<MessageRecipient, Integer> {
}
