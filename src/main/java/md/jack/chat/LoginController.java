package md.jack.chat;

import md.jack.domain.Group;
import md.jack.domain.User;
import md.jack.domain.UserGroup;
import md.jack.event.LoginEvent;
import md.jack.event.ParticipantRepository;
import md.jack.service.GroupServiceImpl;
import md.jack.service.UserGroupServiceImpl;
import md.jack.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class LoginController {

    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private GroupServiceImpl groupService;
    @Autowired
    private UserGroupServiceImpl userGroupService;
    @Autowired
    private ParticipantRepository participantRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @RequestMapping(path = {"/", "/login"}, method = RequestMethod.GET)
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!auth.getName().equals("anonymousUser")) {
            return getChatRoom();
        } else modelAndView.setViewName("index");
        return modelAndView;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView registration() {
        ModelAndView modelAndView = new ModelAndView();
        User user = new User();
        modelAndView.addObject("user", user);
        modelAndView.setViewName("registration");
        return modelAndView;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView createNewUser(@Valid User user, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        User userExists = userService.findUserByEmail(user.getEmail());
        if (userExists != null) {
            bindingResult
                    .rejectValue("email", "error.user",
                                 "There is already a user registered with the name provided"
                    );
        }
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("registration");
        } else {
            userService.saveUser(user);
            modelAndView.addObject("successMessage", "User has been registered successfully");
            modelAndView.addObject("user", new User());
            modelAndView.setViewName("registration");
        }
        return modelAndView;
    }

    @RequestMapping(value = "/group", method = RequestMethod.GET)
    public ModelAndView group() {
        ModelAndView modelAndView = new ModelAndView();
        Group group = new Group();
        modelAndView.addObject("group", group);
        modelAndView.setViewName("group");
        return modelAndView;
    }

    @RequestMapping(value = "/group", method = RequestMethod.POST)
    public ModelAndView createNewGroup(@Valid Group group, BindingResult bindingResult, Principal principal) {
        ModelAndView modelAndView = new ModelAndView();
        Group groupExists = groupService.findGroupByName(group.getName());
        if (groupExists != null) {
            bindingResult
                    .rejectValue("name", "error.user",
                                 "There is already a group registered with the name provided"
                    );
        }
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("group");
        } else {
            UserGroup newUserGroup = new UserGroup();
            newUserGroup.setForeignKeyUserGroupUser(userService.findUserByEmail(principal.getName()));
            newUserGroup.setForeignKeyGroup(group);
            userGroupService.saveUserGroup(newUserGroup);
            groupService.saveGroup(group);
            modelAndView.addObject("successMessage", "Group has been registered successfully");
            modelAndView.addObject("group", new Group());
            modelAndView.setViewName("group");
        }
        return modelAndView;
    }

    @RequestMapping(value = "/group-registration", method = RequestMethod.GET)
    public ModelAndView groupRegister(Principal principal) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("group", new Group());
        List<Group> groups = groupService.findAllGroups().stream()
                .filter(group -> group.getUserGroups()
                        .stream()
                        .noneMatch(userGroup -> userGroup.getForeignKeyUserGroupUser().getEmail().equals(principal.getName())))
                .collect(Collectors.toList());
        modelAndView.addObject("groups", groups);
        modelAndView.setViewName("group-register");
        return modelAndView;
    }

    @RequestMapping(value = "/group-registration", method = RequestMethod.POST)
    public ModelAndView registerToNewGroup(@Valid Group group, BindingResult bindingResult, Principal principal) {
        ModelAndView modelAndView = new ModelAndView();
        Group groupExists = groupService.findGroupByName(group.getName());
        List<Group> groups = groupService.findAllGroups().stream()
                .filter(groupPredicate -> groupPredicate.getUserGroups()
                        .stream()
                        .noneMatch(userGroup -> userGroup.getForeignKeyUserGroupUser().getEmail().equals(principal.getName())))
                .collect(Collectors.toList());
        if (groupExists == null) {
            bindingResult
                    .rejectValue("name", "error.user",
                                 "There is no group registered with the name provided"
                    );
        } else {
            if (!bCryptPasswordEncoder.matches(group.getPassword(), groupExists.getPassword())) {
                bindingResult
                        .rejectValue("password", "error.user",
                                     "Incorrect group password"
                        );
            }
        }
        if (bindingResult.hasErrors()) {
            modelAndView.addObject("groups", groups);
            modelAndView.setViewName("group-register");
        } else {
            UserGroup newUserGroup = new UserGroup();
            newUserGroup.setForeignKeyUserGroupUser(userService.findUserByEmail(principal.getName()));
            newUserGroup.setForeignKeyGroup(groupExists);
            userGroupService.saveUserGroup(newUserGroup);
            modelAndView.addObject("successMessage", "You have been registered successfully this group");
            modelAndView.addObject("group", new Group());
            groups.remove(groupExists);
            modelAndView.addObject("groups", groups);
            modelAndView.setViewName("group-register");
        }
        return modelAndView;
    }

    @RequestMapping(path = "/room")
    public ModelAndView getChatRoom() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        List<Group> groups = user.getUserGroups().stream()
                .map(UserGroup::getForeignKeyGroup)
                .collect(Collectors.toList());
        modelAndView.addObject("userName", user.getName() + " " + user.getLastName());
        Collection<User> allUsers = userService.findAllUsers();
        participantRepository.getActiveSessions().values().forEach(loginEvent -> allUsers.remove(userService.findUserByEmail(loginEvent.getUsername())));
        modelAndView.addObject("users", allUsers);
        modelAndView.addObject("groups", groups);
        modelAndView.setViewName("room");
        return modelAndView;
    }

    @SubscribeMapping("/chat.participants")
    public Collection<LoginEvent> retrieveParticipants() {
        return participantRepository.getActiveSessions().values();
    }
}
