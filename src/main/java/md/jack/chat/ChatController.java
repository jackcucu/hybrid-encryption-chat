package md.jack.chat;

import md.jack.domain.Group;
import md.jack.domain.User;
import md.jack.event.LoginEvent;
import md.jack.event.ParticipantRepository;
import md.jack.messages.ChatMessage;
import md.jack.service.GroupServiceImpl;
import md.jack.service.MessageServiceImpl;
import md.jack.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/chat")
public class ChatController {

    @Autowired
    private SimpMessagingTemplate webSocket;
    @Autowired
    private MessageServiceImpl messageService;
    @Autowired
    private ParticipantRepository participantRepository;
    @Autowired
    private GroupServiceImpl groupService;
    @Autowired
    private UserServiceImpl userService;

    @RequestMapping(method = RequestMethod.POST)
    public ChatMessage postMessage(@RequestBody ChatMessage chatMessage) {
        chatMessage.setTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM HH:mm:ss")));
        User userByEmail = userService.findUserByEmail(chatMessage.getFrom());
        chatMessage.setFromName(userByEmail.getName());
        chatMessage.setFromLastName(userByEmail.getLastName());
        switch (chatMessage.getMessageType()) {
            case USER:
                chatMessage.setFromId(userByEmail.getId());
                webSocket.convertAndSend("/topic/" + chatMessage.getFrom(), chatMessage);
                webSocket.convertAndSend("/topic/" + chatMessage.getTo(), chatMessage);
                messageService.saveUserMessage(chatMessage);
                break;
            case GROUP:
                Collection<LoginEvent> values = participantRepository.getActiveSessions().values();
                Group groupByName = groupService.findGroupByName(chatMessage.getTo());
                chatMessage.setFromId(groupByName.getId());
                List<String> collect = groupByName.getUserGroups()
                        .stream()
                        .map(userGroup -> userGroup.getForeignKeyUserGroupUser().getEmail())
                        .filter(s -> values.contains(new LoginEvent(s)))
                        .collect(Collectors.toList());
                collect.forEach(s -> webSocket.convertAndSend("/topic/" + s, chatMessage));
                messageService.saveGroupMessage(chatMessage);
        }
        return chatMessage;
    }

    @RequestMapping(method = RequestMethod.GET)
    public Collection<ChatMessage> getMessages(@RequestParam(value = "from") String from,
                                               @RequestParam(value = "to") String to,
                                               @RequestParam(value = "type") String type) {
        return messageService.findAllChatMessages(from, to, type);
    }

}
