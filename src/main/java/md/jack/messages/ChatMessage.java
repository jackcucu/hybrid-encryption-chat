package md.jack.messages;

public class ChatMessage {

    private String message;
    private String from;
    private int fromId;
    private String fromName;
    private String fromLastName;
    private String time;
    private MessageType messageType;
    private String to;

    public ChatMessage() {
    }

    public ChatMessage(String message, int fromId, String to, String fromName, String fromLastName, String from, String time, MessageType messageType) {
        this.message = message;
        this.fromId = fromId;
        this.from = from;
        this.fromName = fromName;
        this.fromLastName = fromLastName;
        this.time = time;
        this.to = to;
        this.messageType = messageType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getFromLastName() {
        return fromLastName;
    }

    public void setFromLastName(String fromLastName) {
        this.fromLastName = fromLastName;
    }

    public int getFromId() {
        return fromId;
    }

    public void setFromId(int fromId) {
        this.fromId = fromId;
    }
}
