package md.jack.domain;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "chat_group")
public class Group implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique = true)
    @NotEmpty(message = "*Please provide an name")
    private String name;

    @Length(min = 6, message = "*Your password must have at least 6 characters")
    @NotEmpty(message = "*Please provide your password")
    private String password;
    @OneToMany(mappedBy = "foreignKeyGroup", cascade = CascadeType.ALL)
    private List<UserGroup> userGroups = new ArrayList<>();
    @OneToMany(mappedBy = "foreignKeyMessageRecipientGroup")
    private List<MessageRecipient> messageRecipients = new ArrayList<>();

    public Group() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<UserGroup> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<UserGroup> userGroups) {
        this.userGroups = userGroups;
    }

    public List<MessageRecipient> getMessageRecipients() {
        return messageRecipients;
    }

    public void setMessageRecipients(List<MessageRecipient> messageRecipients) {
        this.messageRecipients = messageRecipients;
    }
}
