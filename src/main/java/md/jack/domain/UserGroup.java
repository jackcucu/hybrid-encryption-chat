package md.jack.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "user_group")
public class UserGroup implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User foreignKeyUserGroupUser;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "group_id")
    private Group foreignKeyGroup;

    public UserGroup() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getForeignKeyUserGroupUser() {
        return foreignKeyUserGroupUser;
    }

    public void setForeignKeyUserGroupUser(User foreignKeyUserGroupUser) {
        this.foreignKeyUserGroupUser = foreignKeyUserGroupUser;
    }

    public Group getForeignKeyGroup() {
        return foreignKeyGroup;
    }

    public void setForeignKeyGroup(Group foreignKeyGroup) {
        this.foreignKeyGroup = foreignKeyGroup;
    }
}
