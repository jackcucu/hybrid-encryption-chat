package md.jack.domain;

import md.jack.messages.MessageType;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "messages")
@Proxy(lazy = false)
public class Message implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String body;

    private LocalDateTime time;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "creator_id")
    private User foreignKeyMessageUser;

    @OneToOne(mappedBy = "foreignKeyMessageRecipientMessage", cascade = CascadeType.ALL)
    private MessageRecipient messageRecipient;

    @OneToMany(mappedBy = "foreignKeyMessageParent", cascade = CascadeType.ALL)
    private List<Message> parentId = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "parent_id")
    private Message foreignKeyMessageParent;

    private boolean isParent;

    private MessageType type;

    private String encryptedKey;

    private String key;

    public Message() {
    }

    public Message(String body) {
        this.body = body;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public User getForeignKeyMessageUser() {
        return foreignKeyMessageUser;
    }

    public void setForeignKeyMessageUser(User foreignKeyMessageUser) {
        this.foreignKeyMessageUser = foreignKeyMessageUser;
    }

    public MessageRecipient getMessageRecipient() {
        return messageRecipient;
    }

    public void setMessageRecipient(MessageRecipient messageRecipient) {
        this.messageRecipient = messageRecipient;
    }

    public List<Message> getParentId() {
        return parentId;
    }

    public void setParentId(List<Message> parentId) {
        this.parentId = parentId;
    }

    public Message getForeignKeyMessageParent() {
        return foreignKeyMessageParent;
    }

    public void setForeignKeyMessageParent(Message foreignKeyMessageParent) {
        this.foreignKeyMessageParent = foreignKeyMessageParent;
    }

    public boolean isParent() {
        return isParent;
    }

    public void setParent(boolean parent) {
        isParent = parent;
    }

    public String getEncryptedKey() {
        return encryptedKey;
    }

    public void setEncryptedKey(final String encryptedKey) {
        this.encryptedKey = encryptedKey;
    }

    public String getKey() {
        return key;
    }

    public void setKey(final String key) {
        this.key = key;
    }
}
