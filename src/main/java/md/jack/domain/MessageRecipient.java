package md.jack.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "message_recipient")
public class MessageRecipient implements Serializable {
    boolean isRead;
    boolean isGroup;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "recipient_id")
    private User foreignKeyMessageRecipientUser;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "recipient_group_id")
    private Group foreignKeyMessageRecipientGroup;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "message_id")
    private Message foreignKeyMessageRecipientMessage;

    public MessageRecipient() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getForeignKeyMessageRecipientUser() {
        return foreignKeyMessageRecipientUser;
    }

    public void setForeignKeyMessageRecipientUser(User foreignKeyMessageRecipientUser) {
        this.foreignKeyMessageRecipientUser = foreignKeyMessageRecipientUser;
    }

    public Group getForeignKeyMessageRecipientGroup() {
        return foreignKeyMessageRecipientGroup;
    }

    public void setForeignKeyMessageRecipientGroup(Group foreignKeyMessageRecipientGroup) {
        this.foreignKeyMessageRecipientGroup = foreignKeyMessageRecipientGroup;
    }

    public Message getForeignKeyMessageRecipientMessage() {
        return foreignKeyMessageRecipientMessage;
    }

    public void setForeignKeyMessageRecipientMessage(Message foreignKeyMessageRecipientMessage) {
        this.foreignKeyMessageRecipientMessage = foreignKeyMessageRecipientMessage;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public boolean isGroup() {
        return isGroup;
    }

    public void setGroup(boolean group) {
        isGroup = group;
    }
}
