package md.jack.domain;

import javax.persistence.*;

@Entity(name = "public_keys")
public class Public {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User foreignKeyUserId;

    private String publicKey;

    @Column(columnDefinition = "TEXT")
    private String privateKey;

    public Public() {
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public User getForeignKeyUserId() {
        return foreignKeyUserId;
    }

    public void setForeignKeyUserId(final User foreignKeyUserId) {
        this.foreignKeyUserId = foreignKeyUserId;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(final String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(final String privateKey) {
        this.privateKey = privateKey;
    }
}
