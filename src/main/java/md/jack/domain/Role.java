package md.jack.domain;

import javax.persistence.*;

@Entity(name = "user_role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_role_id")
    private int id;
    private String role;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "username", referencedColumnName = "email")
    private User foreignKeyRoleUser;

    public Role() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public User getForeignKeyRoleUser() {
        return foreignKeyRoleUser;
    }

    public void setForeignKeyRoleUser(User foreignKeyRoleUser) {
        this.foreignKeyRoleUser = foreignKeyRoleUser;
    }
}
