package md.jack.service;

import md.jack.domain.User;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import static java.util.Base64.getDecoder;
import static java.util.Base64.getEncoder;

@Service
public class EncryptedServiceImpl implements EncryptService {

    @Override
    public String getEncryptedKey(final String key, final User user) {
        final Cipher cipher;
        try {
            cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, getPublic(user.getaPublic().getPublicKey()));

            return getEncoder().encodeToString(cipher.doFinal(getDecoder().decode(key)));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public String getDecryptedKey(final String key, final User user) {
        final Cipher cipher;
        try {
            cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, getPrivate(user.getaPublic().getPrivateKey()));
            return getEncoder().encodeToString(cipher.doFinal(getDecoder().decode(key)));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String getEncryptedMessage(final String key, final String message) {
        final Cipher cipher;
        try {
            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, getSecretKey(key));
            return getEncoder().encodeToString(cipher.doFinal(message.getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public String getDecryptedMessage(final String key, final String message) {
        final Cipher cipher;
        try {
            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, getSecretKey(key));
            return new String(cipher.doFinal(getDecoder().decode(message)));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private PrivateKey getPrivate(final String key) throws Exception {
        byte[] keyBytes = getDecoder().decode(key);
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }

    private PublicKey getPublic(final String key) throws Exception {
        byte[] keyBytes = getDecoder().decode(key);
        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(spec);
    }

    private SecretKeySpec getSecretKey(final String key) throws IOException {
        byte[] keyBytes = getDecoder().decode(key);
        return new SecretKeySpec(keyBytes, "AES");
    }
}
