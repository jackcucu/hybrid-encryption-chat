package md.jack.service;

import md.jack.domain.User;

public interface EncryptService {
    String getEncryptedKey(String key, User user);

    String getDecryptedKey(String key, User user);

    String getEncryptedMessage(String key, String message);

    String getDecryptedMessage(String key, String message);
}
