package md.jack.service;

import md.jack.domain.User;

import java.util.Collection;

public interface UserService {

    Collection<User> findAllUsers();

    User findUserById(Integer id);

    User findUserByEmail(String email);

    void removeUserById(Integer id);

    void updateUser(User user);

    void saveUser(User user);
}
