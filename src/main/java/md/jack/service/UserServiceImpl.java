package md.jack.service;

import md.jack.domain.*;
import md.jack.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Function;

import static java.util.Base64.getEncoder;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroupServiceImpl groupService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private KeyPair keyPair;

    public UserServiceImpl() {
    }

    @Override
    public User findUserByEmail(String email) {
        Collection<User> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);
        return users
                .stream()
                .filter(user -> user.getEmail().equals(email))
                .map(Optional::ofNullable)
                .findFirst()
                .flatMap(Function.identity())
                .orElse(null);
    }

    @Override
    public Collection<User> findAllUsers() {
        Collection<User> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);
        return users;
    }

    @Override
    public User findUserById(Integer id) {
        return userRepository.findOne(id);
    }

    @Override
    public void removeUserById(Integer id) {
        userRepository.delete(id);
    }

    @Override
    public void updateUser(User user) {
        userRepository.save(user);
    }

    @Override
    public void saveUser(User user) {
        try {
            KeyPairGenerator keyGen;
            keyGen = KeyPairGenerator.getInstance("RSA");
            keyGen.initialize(1024);
            keyPair = keyGen.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        Public aPublic = new Public();
        aPublic.setPublicKey(getEncoder().encodeToString(keyPair.getPublic().getEncoded()));
        aPublic.setPrivateKey(getEncoder().encodeToString(keyPair.getPrivate().getEncoded()));
        aPublic.setForeignKeyUserId(user);
        user.setaPublic(aPublic);

        Role userRole = new Role();
        userRole.setRole("ROLE_USER");
        userRole.setForeignKeyRoleUser(user);
        user.getRoles().add(userRole);

        Group group = groupService.findGroupByName("public");
        if (group == null) {
            group = new Group();
            group.setName("public");
            group.setPassword("123456");
        }

        UserGroup userGroup = new UserGroup();
        userGroup.setForeignKeyUserGroupUser(user);
        userGroup.setForeignKeyGroup(group);
        user.getUserGroups().add(userGroup);
        userRepository.save(user);
    }
}
