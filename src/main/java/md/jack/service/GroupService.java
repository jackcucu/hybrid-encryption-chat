package md.jack.service;

import md.jack.domain.Group;

import java.util.Collection;

public interface GroupService {

    Collection<Group> findAllGroups();

    Group findGroupById(Integer id);

    Group findGroupByName(String name);

    void removeGroupById(Integer id);

    void updateGroup(Group group);

    void saveGroup(Group group);
}
