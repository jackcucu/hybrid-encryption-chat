package md.jack.service;

import md.jack.domain.Group;
import md.jack.domain.Message;
import md.jack.domain.MessageRecipient;
import md.jack.domain.User;
import md.jack.messages.ChatMessage;
import md.jack.messages.MessageType;
import md.jack.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.crypto.spec.SecretKeySpec;
import javax.transaction.Transactional;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;

import static java.util.Base64.getEncoder;

@Service
@Transactional
public class MessageServiceImpl implements MessageService {
    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private GroupServiceImpl groupService;

    @Autowired
    private EncryptService encryptService;

    public MessageServiceImpl() {
    }

    @Override
    public Message findMessageParentByUser(User user, User secondUser) {
        Collection<Message> messages = new ArrayList<>();
        messageRepository.findAll().forEach(messages::add);
        return messages.stream()
                .filter(message -> message.getForeignKeyMessageUser().equals(user) || message.getForeignKeyMessageUser().equals(secondUser))
                .filter(Message::isParent)
                .filter(message -> !message.getMessageRecipient().isGroup()
                        && (message.getForeignKeyMessageUser().equals(user)
                        && message.getMessageRecipient().getForeignKeyMessageRecipientUser().equals(secondUser)
                        || (message.getForeignKeyMessageUser().equals(secondUser)
                        && message.getMessageRecipient().getForeignKeyMessageRecipientUser().equals(user))))
                .findAny()
                .orElse(null);
    }

    @Override
    public Message findMessageParentByGroup(User user, Group group) {
        Collection<Message> messages = new ArrayList<>();
        messageRepository.findAll().forEach(messages::add);
        return messages.stream()
                .filter(Message::isParent)
                .filter(message ->
                        {
                            if (message.getMessageRecipient().isGroup()) {
                                boolean match = message.getMessageRecipient().getForeignKeyMessageRecipientGroup().getUserGroups()
                                        .stream()
                                        .anyMatch(userGroup -> userGroup.getForeignKeyUserGroupUser().getId() == user.getId());
                                if (match) {
                                    return true;
                                }
                            } else return false;
                            return false;
                        })
                .filter(message ->
                        {
                            if (message.getMessageRecipient().isGroup()) {
                                if (message.getMessageRecipient().getForeignKeyMessageRecipientGroup().getId() == group.getId()) {
                                    return true;
                                }
                            } else return false;
                            return false;
                        })
                .findAny()
                .orElse(null);
    }

    @Override
    public Collection<Message> findAllMessages() {
        Collection<Message> messages = new ArrayList<>();
        messageRepository.findAll().forEach(messages::add);
        return messages;
    }

    @Override
    public Collection<ChatMessage> findAllChatMessages(String from, String to, String type) {
        Collection<ChatMessage> chatMessages = new ArrayList<>();
        Message message = null;
        final User fromUser = userService.findUserByEmail(from);
        final User toUser = userService.findUserByEmail(to);
        if (type.equalsIgnoreCase("user")) {
            message = this.findMessageParentByUser(fromUser, toUser);
        } else if (type.equalsIgnoreCase(("group"))) {
            message = this.findMessageParentByGroup(fromUser, groupService.findGroupByName(to));
        }
        if (message != null) {
            ChatMessage chatMessage = new ChatMessage();
            chatMessage.setFrom(message.getForeignKeyMessageUser().getEmail());
            setMessage(type, message, chatMessage);
            chatMessage.setTime(message.getTime().format(DateTimeFormatter.ofPattern("dd.MM HH:mm:ss")));
            chatMessage.setMessageType(message.getType());
            chatMessage.setFromName(message.getForeignKeyMessageUser().getName());
            chatMessage.setFromLastName(message.getForeignKeyMessageUser().getLastName());
            chatMessages.add(chatMessage);
            message.getParentId().forEach(message1 ->
                                          {
                                              ChatMessage messageConsumer = new ChatMessage();
                                              messageConsumer.setMessageType(message1.getType());
                                              setMessage(type, message1, messageConsumer);
                                              messageConsumer.setTime(message1.getTime().format(DateTimeFormatter.ofPattern("dd.MM HH:mm:ss")));
                                              messageConsumer.setFrom(message1.getForeignKeyMessageUser().getEmail());
                                              messageConsumer.setFromName(message1.getForeignKeyMessageUser().getName());
                                              messageConsumer.setFromLastName(message1.getForeignKeyMessageUser().getLastName());
                                              chatMessages.add(messageConsumer);
                                          });
        }
        return chatMessages;
    }

    private void setMessage(final String type, final Message message, final ChatMessage chatMessage) {
        if (type.equalsIgnoreCase("user")) {
            User receiver = userService.findUserById(message.getMessageRecipient().getForeignKeyMessageRecipientUser().getId());
            final String decryptedKey = encryptService.getDecryptedKey(message.getEncryptedKey(), receiver);
            final String decryptedMessage = encryptService.getDecryptedMessage(decryptedKey, message.getBody());
            chatMessage.setMessage(decryptedMessage);
        } else {
            chatMessage.setMessage(message.getBody());
        }
    }

    @Override
    public Message findMessageById(Integer id) {
        return messageRepository.findOne(id);
    }

    @Override
    public void removeMessageById(Integer id) {
        messageRepository.delete(id);
    }

    @Override
    public void updateMessage(Message message) {
        messageRepository.save(message);
    }

    @Override
    public void saveUserMessage(ChatMessage message) {
        User fromUserToUser = userService.findUserByEmail(message.getFrom());
        User toUser = userService.findUserByEmail(message.getTo());
        Message messageParentByUser = this.findMessageParentByUser(fromUserToUser, toUser);
        Message messageToSend = new Message();
        if (messageParentByUser != null) {
            messageToSend.setParent(false);
            messageToSend.setForeignKeyMessageParent(messageParentByUser);
        } else {
            messageToSend.setParent(true);
        }
        try {
            SecureRandom rnd = new SecureRandom();
            byte[] bytes = new byte[16];
            rnd.nextBytes(bytes);
            final SecretKeySpec secretKeySpec = new SecretKeySpec(bytes, "AES");

            final String key = getEncoder().encodeToString(secretKeySpec.getEncoded());

            final String secretKeyEncoded = encryptService.getEncryptedKey(key, toUser);

            final String encryptedMessage = encryptService.getEncryptedMessage(key, message.getMessage());

            messageToSend.setKey(key);
            messageToSend.setEncryptedKey(secretKeyEncoded);
            messageToSend.setBody(encryptedMessage);
            messageToSend.setType(MessageType.USER);
            messageToSend.setTime(LocalDateTime.now());
            messageToSend.setForeignKeyMessageUser(fromUserToUser);
            MessageRecipient messageRecipient = new MessageRecipient();
            messageRecipient.setForeignKeyMessageRecipientMessage(messageToSend);
            messageRecipient.setForeignKeyMessageRecipientUser(toUser);
            messageToSend.setMessageRecipient(messageRecipient);
            messageRepository.save(messageToSend);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Async
    @Override
    public void saveGroupMessage(ChatMessage message) {
        User fromUserToGroup = userService.findUserByEmail(message.getFrom());
        Group toGroup = groupService.findGroupByName(message.getTo());
        Message messageParentByGroup = this.findMessageParentByGroup(fromUserToGroup, toGroup);
        Message messageToSend = new Message();
        if (messageParentByGroup != null) {
            messageToSend.setParent(false);
            messageToSend.setForeignKeyMessageParent(messageParentByGroup);
        } else {
            messageToSend.setParent(true);
        }
        messageToSend.setBody(message.getMessage());
        messageToSend.setForeignKeyMessageUser(fromUserToGroup);
        messageToSend.setType(MessageType.GROUP);
        messageToSend.setTime(LocalDateTime.now());
        MessageRecipient messageRecipientGroup = new MessageRecipient();
        messageRecipientGroup.setForeignKeyMessageRecipientMessage(messageToSend);
        messageRecipientGroup.setForeignKeyMessageRecipientGroup(toGroup);
        messageRecipientGroup.setGroup(true);
        messageToSend.setMessageRecipient(messageRecipientGroup);
        messageRepository.save(messageToSend);
    }


}
