package md.jack.service;

import md.jack.domain.Group;
import md.jack.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Function;

@Service
@Transactional
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public GroupServiceImpl() {
    }

    @Override
    public Group findGroupByName(String name) {
        Collection<Group> groups = new ArrayList<>();
        groupRepository.findAll().forEach(groups::add);
        return groups
                .stream()
                .filter(group -> group.getName().equals(name))
                .map(Optional::ofNullable)
                .findFirst()
                .flatMap(Function.identity())
                .orElse(null);
    }

    @Override
    public Collection<Group> findAllGroups() {
        Collection<Group> groups = new ArrayList<>();
        groupRepository.findAll().forEach(groups::add);
        return groups;
    }

    @Override
    public Group findGroupById(Integer id) {
        return groupRepository.findOne(id);
    }

    @Override
    public void removeGroupById(Integer id) {
        groupRepository.delete(id);
    }

    @Override
    public void updateGroup(Group group) {
        groupRepository.save(group);
    }

    @Override
    public void saveGroup(Group group) {
        group.setPassword(bCryptPasswordEncoder.encode(group.getPassword()));
        groupRepository.save(group);
    }
}
