package md.jack.service;

import md.jack.domain.Group;
import md.jack.domain.Message;
import md.jack.domain.User;
import md.jack.messages.ChatMessage;

import java.util.Collection;

public interface MessageService {

    Collection<Message> findAllMessages();

    Collection<ChatMessage> findAllChatMessages(String from, String to, String type);

    Message findMessageById(Integer id);

    Message findMessageParentByUser(User user, User user2);

    Message findMessageParentByGroup(User user, Group group);

    void removeMessageById(Integer id);

    void updateMessage(Message message);

    void saveUserMessage(ChatMessage message);

    void saveGroupMessage(ChatMessage message);
}
