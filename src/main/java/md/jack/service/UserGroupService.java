package md.jack.service;

import md.jack.domain.UserGroup;

import java.util.Collection;

public interface UserGroupService {

    Collection<UserGroup> findAllUserGroups();

    UserGroup findUserGroupById(Integer id);

    void removeUserGroupById(Integer id);

    void updateUserGroup(UserGroup userGroup);

    void saveUserGroup(UserGroup userGroup);
}
