package md.jack.service;

import md.jack.domain.MessageRecipient;

import java.util.Collection;

public interface MessageRecipientService {

    Collection<MessageRecipient> findAllMessageRecipients();

    MessageRecipient findMessageRecipientById(Integer id);

    Collection<MessageRecipient> findMessageRecipientByUserId(Integer id);

    MessageRecipient findMessageRecipientByName(String name);

    void removeMessageRecipientById(Integer id);

    void updateMessageRecipient(MessageRecipient message);

    void saveMessageRecipient(MessageRecipient message);
}
