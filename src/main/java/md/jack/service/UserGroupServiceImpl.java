package md.jack.service;

import md.jack.domain.UserGroup;
import md.jack.repository.UserGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;

@Service
@Transactional
public class UserGroupServiceImpl implements UserGroupService {

    @Autowired
    private UserGroupRepository userGroupRepository;

    public UserGroupServiceImpl() {
    }

    @Override
    public Collection<UserGroup> findAllUserGroups() {
        Collection<UserGroup> userGroups = new ArrayList<>();
        userGroupRepository.findAll().forEach(userGroups::add);
        return userGroups;
    }

    @Override
    public UserGroup findUserGroupById(Integer id) {
        return userGroupRepository.findOne(id);
    }

    @Override
    public void removeUserGroupById(Integer id) {
        userGroupRepository.delete(id);
    }

    @Override
    public void updateUserGroup(UserGroup userGroup) {
        userGroupRepository.save(userGroup);
    }

    @Override
    public void saveUserGroup(UserGroup userGroup) {
        userGroupRepository.save(userGroup);
    }
}
