package md.jack.service;

import md.jack.domain.MessageRecipient;
import md.jack.repository.MessageRecipientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
@Transactional
public class MessageRecipientServiceImpl implements MessageRecipientService {

    @Autowired
    private MessageRecipientRepository messageRecipientRepository;

    public MessageRecipientServiceImpl() {
    }

    @Override
    public Collection<MessageRecipient> findAllMessageRecipients() {
        Collection<MessageRecipient> messageRecipients = new ArrayList<>();
        messageRecipientRepository.findAll().forEach(messageRecipients::add);
        return messageRecipients;
    }

    @Override
    public MessageRecipient findMessageRecipientById(Integer id) {
        return messageRecipientRepository.findOne(id);
    }

    @Override
    public ArrayList<MessageRecipient> findMessageRecipientByUserId(Integer id) {
        Collection<MessageRecipient> messageRecipients = new ArrayList<>();
        messageRecipientRepository.findAll().forEach(messageRecipients::add);
        return (ArrayList<MessageRecipient>) messageRecipients
                .stream()
                .filter(m -> m.getForeignKeyMessageRecipientUser() != null)
                .filter(m -> m.getForeignKeyMessageRecipientUser().getId() == id)
                .collect(Collectors.toList());
    }

    @Override
    public MessageRecipient findMessageRecipientByName(String name) {
        return null;
    }

    @Override
    public void removeMessageRecipientById(Integer id) {
        messageRecipientRepository.delete(id);
    }

    @Override
    public void updateMessageRecipient(MessageRecipient messageRecipient) {
        messageRecipientRepository.save(messageRecipient);
    }

    @Override
    public void saveMessageRecipient(MessageRecipient messageRecipient) {
        messageRecipientRepository.save(messageRecipient);
    }
}
