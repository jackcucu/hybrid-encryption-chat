package md.jack.event;

import java.util.Date;
import java.util.Objects;

public class LoginEvent {

    public Integer id;
    public String username;
    public Date time;
    public String name;
    public String lastName;

    public LoginEvent(String username) {
        this.username = username;
    }

    public LoginEvent(String username, String name, String lastName, Integer id) {
        this.username = username;
        this.name = name;
        this.lastName = lastName;
        this.id = id;
        this.time = new Date();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoginEvent)) return false;
        LoginEvent that = (LoginEvent) o;
        return Objects.equals(getUsername(), that.getUsername());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUsername());
    }
}
