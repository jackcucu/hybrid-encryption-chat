package md.jack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppooChatServerModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppooChatServerModuleApplication.class, args);
    }
}
